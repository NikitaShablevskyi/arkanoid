import { Ball } from './sprites/Ball';
import { Brick } from './sprites/Brick';
import { Paddle } from './sprites/Paddle';
import { CanvasView } from './view/CanvasView';
import { DisplayView } from './view/DisplayView';
import { Collision } from './game/Collison';
import { randomInBoundaries } from './helpers';
import {
  BALL_MAX_SPEED,
  BALL_MIN_SPEED,
  BALL_SIZE,
  BALL_STARTX,
  BALL_STARTY,
  BRICK_ENERGY,
  BRICK_HEIGHT,
  BRICK_IMAGES,
  BRICK_PADDING,
  BRICK_WIDTH,
  BRICKS_ARRAY_LENGTH, BRICKS_MAX_ARRAY,
  BRICKS_MIN_ARRAY,
  PADDLE_HEIGHT,
  PADDLE_MAX_SPEED,
  PADDLE_MIN_SPEED,
  PADDLE_PADDING,
  PADDLE_STARTX,
  PADDLE_WIDTH,
  PADDLE_Y_BOTTOM_LIMIT,
  PADDLE_Y_TOP_LIMIT,
  STAGE_COLS,
  STAGE_PADDING
} from './setup/setup';

import BALL_IMAGE from './assets/ball.png';
import PADDLE_IMAGE from './assets/paddle.png';

export class Game {
  gameOver = false;
  score = 0;
  ball!: Ball;
  bricks!: Brick[];
  paddle!: Paddle;
  collision: Collision;

  constructor() {
    this.collision = new Collision();
  }

  start(playground: CanvasView, displayView: DisplayView): void {
    this.gameOver = false;

    this.bricks = this.createBricks(
      new Array((BRICKS_MIN_ARRAY + randomInBoundaries(0, BRICKS_MAX_ARRAY - BRICKS_MIN_ARRAY)) * BRICKS_ARRAY_LENGTH)
        .fill(0)
        .map(() => randomInBoundaries(0, 5)));

    const ballSpeed = randomInBoundaries(BALL_MIN_SPEED, BALL_MAX_SPEED);
    const paddleSpeed = randomInBoundaries(PADDLE_MIN_SPEED, PADDLE_MAX_SPEED);

    this.ball = new Ball(
      BALL_SIZE,
      ballSpeed,
      { x: BALL_STARTX, y: BALL_STARTY },
      BALL_IMAGE
    );

    this.paddle = new Paddle(
      paddleSpeed,
      PADDLE_WIDTH,
      PADDLE_HEIGHT,
      {
        x: PADDLE_STARTX,
        y: playground.canvas.height - PADDLE_HEIGHT - PADDLE_PADDING
      },
      PADDLE_IMAGE
    );

    displayView.score = this.score;
    displayView.setGameMode();
    displayView.ballSpeed = ballSpeed;
    displayView.paddleSpeed = paddleSpeed;

    this.loop(playground, displayView);
  }

  loop(playground: CanvasView, displayView: DisplayView): void {
    playground.clear();
    playground.drawBricks(this.bricks);
    playground.drawSprite(this.paddle);
    playground.drawSprite(this.ball);
    // Move Ball
    this.ball.moveBall();

    // Move paddle and check so it won't exit the playfield
    if (this.canMovePaddle(playground)) {
      this.paddle.movePaddle();
    }

    this.collision.checkBallCollision(this.ball, this.paddle, playground);
    const collidingBrick = this.collision.isCollidingBricks(this.ball, this.bricks);

    if (collidingBrick) {
      this.score += 1;
      displayView.score = this.score;
    }

    // Game Over when ball leaves playField
    if (this.ball.pos.y > playground.canvas.height) {
      this.gameOver = true;
      displayView.score = this.score;
      displayView.highScore = displayView.highScore < this.score ? this.score : displayView.highScore;
      this.score = 0;
    }
    // If game won, set gameOver and display win
    if (this.bricks.length === 0) {
      displayView.info = 'Game Won!';
      this.gameOver = false;
      displayView.setDefaultMode();
      return;
    }
    // Return if gameover and don't run the requestAnimationFrame
    if (this.gameOver) {
      displayView.info = 'Game Over!';
      this.gameOver = true;
      displayView.setDefaultMode();
      return;
    }

    requestAnimationFrame(() => this.loop(playground, displayView));

  }

  private createBricks(array: number[]): Brick[] {
    return array.reduce((ack, element, i) => {
      const row = Math.floor((i + 1) / STAGE_COLS);
      const col = i % STAGE_COLS;

      const x = STAGE_PADDING + col * (BRICK_WIDTH + BRICK_PADDING);
      const y = STAGE_PADDING + row * (BRICK_HEIGHT + BRICK_PADDING);

      if (element === 0) return ack;

      return [
        ...ack,
        new Brick(
          BRICK_ENERGY[ element ],
          BRICK_WIDTH,
          BRICK_HEIGHT,
          { x, y },
          BRICK_IMAGES[ element ]
        )
      ];
    }, [] as Brick[]);
  };

  private canMovePaddle(playground: CanvasView): boolean {
    return (!this.paddle.isMovingLeft || (this.paddle.isMovingLeft && this.paddle.pos.x > 0)) &&
      (!this.paddle.isMovingRight || (this.paddle.isMovingRight && this.paddle.pos.x < playground.canvas.width - this.paddle.width))
      && (!this.paddle.isMovingTop || (this.paddle.isMovingTop && this.paddle.pos.y > PADDLE_Y_TOP_LIMIT))
      && (!this.paddle.isMovingDown || (this.paddle.isMovingDown && this.paddle.pos.y < PADDLE_Y_BOTTOM_LIMIT));
  }
}
