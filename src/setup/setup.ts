import RED_BRICK_IMAGE from '../assets/bricks/brick-red.png';
import BLUE_BRICK_IMAGE from '../assets/bricks/brick-blue.png';
import GREEN_BRICK_IMAGE from '../assets/bricks/brick-green.png';
import YELLOW_BRICK_IMAGE from '../assets/bricks/brick-yellow.png';
import PURPLE_BRICK_IMAGE from '../assets/bricks/brick-purple.png';

// Grab the canvas element for calculating the brick width
// depending on canvas width
const canvas: HTMLCanvasElement = document.querySelector('#playField') as HTMLCanvasElement;

// Constants
export const STAGE_PADDING = 10;
export const STAGE_ROWS = 20;
export const STAGE_COLS = 10;
export const BRICK_PADDING = 5;
export const BRICK_WIDTH = Math.floor((canvas.width - STAGE_PADDING * 2) / STAGE_COLS) - BRICK_PADDING;
export const BRICK_HEIGHT = Math.floor((canvas.height - STAGE_PADDING * 2) / STAGE_ROWS) - BRICK_PADDING;

export const PADDLE_PADDING = 25;

export const PADDLE_WIDTH = 70;
export const PADDLE_HEIGHT = 10;
export const PADDLE_STARTX = (canvas.width - PADDLE_WIDTH) / 2;

export const PADDLE_Y_TOP_LIMIT = 450;
export const PADDLE_Y_BOTTOM_LIMIT = canvas.height - PADDLE_HEIGHT - PADDLE_PADDING;

export const BALL_SIZE = 15;
export const BALL_STARTX = (canvas.width - PADDLE_WIDTH);
export const BALL_STARTY = canvas.height - PADDLE_HEIGHT - PADDLE_PADDING - BALL_SIZE;

export const BRICKS_ARRAY_LENGTH = 9;
export const BRICKS_MIN_ARRAY = 2;
export const BRICKS_MAX_ARRAY = 9;
export const BALL_MIN_SPEED = 5;
export const BALL_MAX_SPEED = 10;

export const PADDLE_MIN_SPEED = 6;
export const PADDLE_MAX_SPEED = 12;

export const BRICK_IMAGES: { [ key: number ]: string } = {
  1: RED_BRICK_IMAGE,
  2: GREEN_BRICK_IMAGE,
  3: YELLOW_BRICK_IMAGE,
  4: BLUE_BRICK_IMAGE,
  5: PURPLE_BRICK_IMAGE
};

export const BRICK_ENERGY: { [ key: number ]: number } = {
  1: 1, // Red brick
  2: 2, // Green brick
  3: 4, // Yellow brick
  4: 4, // Blue brick
  5: 8 // Purple brick
};
