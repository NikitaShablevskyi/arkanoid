import { Vector } from '../_types/types';
import { GameElement } from './Game-Element';

export class Brick extends GameElement {
  constructor(
    private brickEnergy: number,
    brickWidth: number,
    brickHeight: number,
    position: Vector,
    image: string
  ) {
    super(position, brickWidth, brickHeight, image);
  }

  // Getters

  get energy(): number {
    return this.brickEnergy;
  }

  // Setter
  set energy(energy: number) {
    this.brickEnergy = energy;
  }
}
