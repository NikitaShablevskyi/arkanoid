import { Vector } from '../_types/types';
import { GameElement } from './Game-Element';

export class Paddle extends GameElement {
  private moveLeft: boolean;
  private moveRight: boolean;
  private moveTop: boolean;
  private moveDown: boolean;

  constructor(
    private speed: number,
    paddleWidth: number,
    paddleHeight: number,
    position: Vector,
    image: string
  ) {
    super(position, paddleWidth, paddleHeight, image);
    this.moveLeft = false;
    this.moveRight = false;
    this.moveTop = false;
    this.moveDown = false;

    // Event Listeners
    document.addEventListener('keydown', this.handleMove(true));
    document.addEventListener('keyup', this.handleMove(false));
  }

  // Getters

  get isMovingLeft(): boolean {
    return this.moveLeft;
  }

  get isMovingRight(): boolean {
    return this.moveRight;
  }

  get isMovingDown(): boolean {
    return this.moveDown;
  }

  get isMovingTop(): boolean {
    return this.moveTop;
  }

  movePaddle(): void {
    if (this.moveLeft) {
      this.pos.x -= this.speed;
    }

    if (this.moveRight) {
      this.pos.x += this.speed;
    }

    if (this.moveDown) {
      this.pos.y += this.speed;
    }
    if (this.moveTop) {
      this.pos.y -= this.speed;
    }
  }


  handleMove(pressed: boolean): (e: KeyboardEvent) => void {
    return (e: KeyboardEvent): void => {
      e.preventDefault();
      if (e.code === 'ArrowLeft' || e.key === 'ArrowLeft') {
        this.moveLeft = pressed;
      }
      if (e.code === 'ArrowRight' || e.key === 'ArrowRight') {
        this.moveRight = pressed;
      }

      if (e.code === 'ArrowDown' || e.key === 'ArrowDown') {
        this.moveDown = pressed;
      }
      if (e.code === 'ArrowUp' || e.key === 'ArrowUp') {
        this.moveTop = pressed;
      }
    };
  }
}
