import { Vector } from '../_types/types';

export class GameElement {
  private _image: HTMLImageElement = new Image();

  constructor(private _position: Vector,
              private _width: number,
              private _height: number,
              image: string,) {
    this._image.src = image;
  }

  get image(): HTMLImageElement {
    return this._image;
  }

  get width(): number {
    return this._width;
  }

  get height(): number {
    return this._height;
  }

  get pos(): Vector {
    return this._position;
  }
}
