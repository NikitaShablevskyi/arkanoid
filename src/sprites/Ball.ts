import { Vector } from '../_types/types';
import { GameElement } from './Game-Element';

export class Ball extends GameElement{
  private speed: Vector;

  constructor(
    ballSize: number,
    speed: number,
    position: Vector,
    image: string
  ) {
    super(position, ballSize, ballSize, image);
    this.speed = {
      x: speed,
      y: -speed
    };
  }
  // Methods
  changeYDirection(): void {
    this.speed.y = -this.speed.y;
  }

  changeXDirection(): void {
    this.speed.x = -this.speed.x;
  }

  moveBall(): void {
    this.pos.x += this.speed.x;
    this.pos.y += this.speed.y;
  }
}
