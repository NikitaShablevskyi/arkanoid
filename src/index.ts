import { Game } from './game';
import { CanvasView } from './view/CanvasView';
import { DisplayView } from './view/DisplayView';

const canvas = new CanvasView('#playField');
const displayView = new DisplayView('#display');

const game = new Game();

displayView.startButton.addEventListener('click', () => {
  game.start(canvas, displayView);
});
