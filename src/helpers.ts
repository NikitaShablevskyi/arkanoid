const setAttributes = (element: HTMLElement, attributes: Record<string, unknown>) => {
  Object.entries(attributes).forEach(([ key, value ]) => {
    // @ts-ignore
    element[key] = value;
  })
}

export const createElement = (tagName: string, parentNode: HTMLElement, attributes: Record<string, unknown>) => {
  const element = document.createElement(tagName);
  parentNode.appendChild(element);

  if (attributes) {
    setAttributes(element, attributes);
  }
  return element;
}

export const randomInBoundaries = (min: number, max: number) => {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};
