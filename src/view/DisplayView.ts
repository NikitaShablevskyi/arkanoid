import { createElement } from '../helpers';
import { BALL_MIN_SPEED, PADDLE_MIN_SPEED } from '../setup/setup';

export class DisplayView {
  private scorePrefix = '0000000';

  private _score!: HTMLElement;
  private _highScore!: HTMLElement;
  private _info!: HTMLElement;
  private _start!: HTMLElement;
  private _ballSpeed!: HTMLElement;
  private _paddleSpeed!: HTMLElement;

  private container: HTMLElement;

  constructor(containerName: string) {
    this.container = document.querySelector(containerName) as HTMLDivElement;
    this.clear();
    this.renderScore(this.highScore || 0, 'High Score');
    this.renderScore(0, 'Score');
    this.renderBallSpeed(0);
    this.renderPaddleSpeed(0);
    this.renderInfo('Press Start to play!');
    this.renderStartButton();
  }

  renderScore(data: number, label: 'High Score' | 'Score') {
    const info = createElement('div', this.container, {
      className: 'main__info'
    });

    createElement('div', info, {
      className: 'main__heading',
      textContent: label
    });


    if (label === 'Score') {
      this._score = createElement('div', info, {
        className: 'main__text',
        textContent: this.parseScore(data)
      });
    } else {
      this._highScore = createElement('div', info, {
        className: 'main__text',
        textContent: this.parseScore(data)
      });
    }
  }

  renderBallSpeed(data: number) {
    const speed = createElement('div', this.container, {
      className: 'main__info'
    });

    createElement('div', speed, {
      className: 'main__heading',
      textContent: 'Ball Speed'
    });


    this._ballSpeed = createElement('div', speed, {
      className: 'main__text',
      textContent: this.parseSpeed(data, BALL_MIN_SPEED)
    });
  }

  renderPaddleSpeed(data: number) {
    const speed = createElement('div', this.container, {
      className: 'main__info'
    });

    createElement('div', speed, {
      className: 'main__heading',
      textContent: 'Paddle Speed'
    });


    this._paddleSpeed = createElement('div', speed, {
      className: 'main__text',
      textContent: this.parseSpeed(data , PADDLE_MIN_SPEED)
    });
  }

  renderInfo(text: string) {
    const info = createElement('div', this.container, {
      className: 'main__info'
    });

    this._info = createElement('div', info, {
      className: 'main__heading',
      textContent: text
    });
  }

  renderStartButton(): void {
    this._start = createElement('button', this.container, {
      className: 'main__start-button',
      textContent: 'Start'
    });
  }

  set ballSpeed(data: number) {
    this._ballSpeed.innerText = this.parseSpeed(data, BALL_MIN_SPEED);
  }

  set paddleSpeed(data: number) {
    this._paddleSpeed.innerText = this.parseSpeed(data , PADDLE_MIN_SPEED);
  }

  get startButton(): HTMLElement {
    return this._start;
  }

  get highScore(): number {
    return Number(localStorage.getItem('high')) || 0;
  }

  set highScore(data: number) {
    localStorage.setItem('high', data.toString());

    this._highScore.innerText = this.parseScore(data).toString();
  }

  set score(data: number) {
    this._score.innerText = this.parseScore(data).toString();
  }

  set info(text: string) {
    this._info.innerText = text;
  }

  setGameMode(): void {
    this._start.classList.add('hide');
    this._info.classList.add('hide');
  }

  setDefaultMode(): void {
    this._start.classList.remove('hide');
    this._info.classList.remove('hide');
  }

  private parseSpeed(data: number, min: number): string {
    return `${(data / min).toFixed(2)}x`;
  }

  private parseScore(data: number): string {
    return `${(this.scorePrefix + data).slice(-this.scorePrefix.length)}`;
  }

  private clear(): void {
    while (this.container.firstChild) {
      this.container.firstChild.remove();
    }
  }
}
