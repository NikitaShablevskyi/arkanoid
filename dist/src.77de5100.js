// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles
parcelRequire = (function (modules, cache, entry, globalName) {
  // Save the require from previous bundle to this closure if any
  var previousRequire = typeof parcelRequire === 'function' && parcelRequire;
  var nodeRequire = typeof require === 'function' && require;

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire = typeof parcelRequire === 'function' && parcelRequire;
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error('Cannot find module \'' + name + '\'');
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = cache[name] = new newRequire.Module(name);

      modules[name][0].call(module.exports, localRequire, module, module.exports, this);
    }

    return cache[name].exports;

    function localRequire(x){
      return newRequire(localRequire.resolve(x));
    }

    function resolve(x){
      return modules[name][1][x] || x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [function (require, module) {
      module.exports = exports;
    }, {}];
  };

  var error;
  for (var i = 0; i < entry.length; i++) {
    try {
      newRequire(entry[i]);
    } catch (e) {
      // Save first error but execute all entries
      if (!error) {
        error = e;
      }
    }
  }

  if (entry.length) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(entry[entry.length - 1]);

    // CommonJS
    if (typeof exports === "object" && typeof module !== "undefined") {
      module.exports = mainExports;

    // RequireJS
    } else if (typeof define === "function" && define.amd) {
     define(function () {
       return mainExports;
     });

    // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }

  // Override the current require with this new one
  parcelRequire = newRequire;

  if (error) {
    // throw error from earlier, _after updating parcelRequire_
    throw error;
  }

  return newRequire;
})({"sprites/Game-Element.ts":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.GameElement = void 0;

var GameElement =
/** @class */
function () {
  function GameElement(_position, _width, _height, image) {
    this._position = _position;
    this._width = _width;
    this._height = _height;
    this._image = new Image();
    this._image.src = image;
  }

  Object.defineProperty(GameElement.prototype, "image", {
    get: function get() {
      return this._image;
    },
    enumerable: false,
    configurable: true
  });
  Object.defineProperty(GameElement.prototype, "width", {
    get: function get() {
      return this._width;
    },
    enumerable: false,
    configurable: true
  });
  Object.defineProperty(GameElement.prototype, "height", {
    get: function get() {
      return this._height;
    },
    enumerable: false,
    configurable: true
  });
  Object.defineProperty(GameElement.prototype, "pos", {
    get: function get() {
      return this._position;
    },
    enumerable: false,
    configurable: true
  });
  return GameElement;
}();

exports.GameElement = GameElement;
},{}],"sprites/Ball.ts":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Ball = void 0;

var _GameElement = require("./Game-Element");

var __extends = void 0 && (void 0).__extends || function () {
  var _extendStatics = function extendStatics(d, b) {
    _extendStatics = Object.setPrototypeOf || {
      __proto__: []
    } instanceof Array && function (d, b) {
      d.__proto__ = b;
    } || function (d, b) {
      for (var p in b) {
        if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p];
      }
    };

    return _extendStatics(d, b);
  };

  return function (d, b) {
    _extendStatics(d, b);

    function __() {
      this.constructor = d;
    }

    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
  };
}();

var Ball =
/** @class */
function (_super) {
  __extends(Ball, _super);

  function Ball(ballSize, speed, position, image) {
    var _this = _super.call(this, position, ballSize, ballSize, image) || this;

    _this.speed = {
      x: speed,
      y: -speed
    };
    return _this;
  } // Methods


  Ball.prototype.changeYDirection = function () {
    this.speed.y = -this.speed.y;
  };

  Ball.prototype.changeXDirection = function () {
    this.speed.x = -this.speed.x;
  };

  Ball.prototype.moveBall = function () {
    this.pos.x += this.speed.x;
    this.pos.y += this.speed.y;
  };

  return Ball;
}(_GameElement.GameElement);

exports.Ball = Ball;
},{"./Game-Element":"sprites/Game-Element.ts"}],"sprites/Brick.ts":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Brick = void 0;

var _GameElement = require("./Game-Element");

var __extends = void 0 && (void 0).__extends || function () {
  var _extendStatics = function extendStatics(d, b) {
    _extendStatics = Object.setPrototypeOf || {
      __proto__: []
    } instanceof Array && function (d, b) {
      d.__proto__ = b;
    } || function (d, b) {
      for (var p in b) {
        if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p];
      }
    };

    return _extendStatics(d, b);
  };

  return function (d, b) {
    _extendStatics(d, b);

    function __() {
      this.constructor = d;
    }

    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
  };
}();

var Brick =
/** @class */
function (_super) {
  __extends(Brick, _super);

  function Brick(brickEnergy, brickWidth, brickHeight, position, image) {
    var _this = _super.call(this, position, brickWidth, brickHeight, image) || this;

    _this.brickEnergy = brickEnergy;
    return _this;
  }

  Object.defineProperty(Brick.prototype, "energy", {
    // Getters
    get: function get() {
      return this.brickEnergy;
    },
    // Setter
    set: function set(energy) {
      this.brickEnergy = energy;
    },
    enumerable: false,
    configurable: true
  });
  return Brick;
}(_GameElement.GameElement);

exports.Brick = Brick;
},{"./Game-Element":"sprites/Game-Element.ts"}],"sprites/Paddle.ts":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Paddle = void 0;

var _GameElement = require("./Game-Element");

var __extends = void 0 && (void 0).__extends || function () {
  var _extendStatics = function extendStatics(d, b) {
    _extendStatics = Object.setPrototypeOf || {
      __proto__: []
    } instanceof Array && function (d, b) {
      d.__proto__ = b;
    } || function (d, b) {
      for (var p in b) {
        if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p];
      }
    };

    return _extendStatics(d, b);
  };

  return function (d, b) {
    _extendStatics(d, b);

    function __() {
      this.constructor = d;
    }

    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
  };
}();

var Paddle =
/** @class */
function (_super) {
  __extends(Paddle, _super);

  function Paddle(speed, paddleWidth, paddleHeight, position, image) {
    var _this = _super.call(this, position, paddleWidth, paddleHeight, image) || this;

    _this.speed = speed;
    _this.moveLeft = false;
    _this.moveRight = false;
    _this.moveTop = false;
    _this.moveDown = false; // Event Listeners

    document.addEventListener('keydown', _this.handleMove(true));
    document.addEventListener('keyup', _this.handleMove(false));
    return _this;
  }

  Object.defineProperty(Paddle.prototype, "isMovingLeft", {
    // Getters
    get: function get() {
      return this.moveLeft;
    },
    enumerable: false,
    configurable: true
  });
  Object.defineProperty(Paddle.prototype, "isMovingRight", {
    get: function get() {
      return this.moveRight;
    },
    enumerable: false,
    configurable: true
  });
  Object.defineProperty(Paddle.prototype, "isMovingDown", {
    get: function get() {
      return this.moveDown;
    },
    enumerable: false,
    configurable: true
  });
  Object.defineProperty(Paddle.prototype, "isMovingTop", {
    get: function get() {
      return this.moveTop;
    },
    enumerable: false,
    configurable: true
  });

  Paddle.prototype.movePaddle = function () {
    if (this.moveLeft) {
      this.pos.x -= this.speed;
    }

    if (this.moveRight) {
      this.pos.x += this.speed;
    }

    if (this.moveDown) {
      this.pos.y += this.speed;
    }

    if (this.moveTop) {
      this.pos.y -= this.speed;
    }
  };

  Paddle.prototype.handleMove = function (pressed) {
    var _this = this;

    return function (e) {
      e.preventDefault();

      if (e.code === 'ArrowLeft' || e.key === 'ArrowLeft') {
        _this.moveLeft = pressed;
      }

      if (e.code === 'ArrowRight' || e.key === 'ArrowRight') {
        _this.moveRight = pressed;
      }

      if (e.code === 'ArrowDown' || e.key === 'ArrowDown') {
        _this.moveDown = pressed;
      }

      if (e.code === 'ArrowUp' || e.key === 'ArrowUp') {
        _this.moveTop = pressed;
      }
    };
  };

  return Paddle;
}(_GameElement.GameElement);

exports.Paddle = Paddle;
},{"./Game-Element":"sprites/Game-Element.ts"}],"game/Collison.ts":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Collision = void 0;

var Collision =
/** @class */
function () {
  function Collision() {}

  Collision.prototype.isCollidingBrick = function (ball, brick) {
    if (ball.pos.x < brick.pos.x + brick.width && ball.pos.x + ball.width > brick.pos.x && ball.pos.y < brick.pos.y + brick.height && ball.pos.y + ball.height > brick.pos.y) {
      return true;
    }

    return false;
  }; // Check ball collision with bricks


  Collision.prototype.isCollidingBricks = function (ball, bricks) {
    var _this = this;

    var colliding = false;
    bricks.forEach(function (brick, i) {
      if (_this.isCollidingBrick(ball, brick)) {
        ball.changeYDirection();

        if (brick.energy === 1) {
          bricks.splice(i, 1);
        } else {
          brick.energy -= 1;
        }

        colliding = true;
      }
    });
    return colliding;
  };

  Collision.prototype.checkBallCollision = function (ball, paddle, view) {
    // 1. Check ball collision with paddle
    if (ball.pos.x + ball.width > paddle.pos.x && ball.pos.x < paddle.pos.x + paddle.width && Math.abs(ball.pos.y + ball.height - paddle.pos.y) <= 3) {
      ball.changeYDirection();
    } // 2. Check ball collision with walls
    // Ball movement X constraints


    if (ball.pos.x > view.canvas.width - ball.width || ball.pos.x < 0) {
      ball.changeXDirection();
    } // Ball movement Y constraints


    if (ball.pos.y < 0) {
      ball.changeYDirection();
    }
  };

  return Collision;
}();

exports.Collision = Collision;
},{}],"helpers.ts":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.randomInBoundaries = exports.createElement = void 0;

var setAttributes = function setAttributes(element, attributes) {
  Object.entries(attributes).forEach(function (_a) {
    var key = _a[0],
        value = _a[1]; // @ts-ignore

    element[key] = value;
  });
};

var createElement = function createElement(tagName, parentNode, attributes) {
  var element = document.createElement(tagName);
  parentNode.appendChild(element);

  if (attributes) {
    setAttributes(element, attributes);
  }

  return element;
};

exports.createElement = createElement;

var randomInBoundaries = function randomInBoundaries(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

exports.randomInBoundaries = randomInBoundaries;
},{}],"assets/bricks/brick-red.png":[function(require,module,exports) {
module.exports = "/brick-red.076d5708.png";
},{}],"assets/bricks/brick-blue.png":[function(require,module,exports) {
module.exports = "/brick-blue.a37614ca.png";
},{}],"assets/bricks/brick-green.png":[function(require,module,exports) {
module.exports = "/brick-green.84c4f054.png";
},{}],"assets/bricks/brick-yellow.png":[function(require,module,exports) {
module.exports = "/brick-yellow.649869e5.png";
},{}],"assets/bricks/brick-purple.png":[function(require,module,exports) {
module.exports = "/brick-purple.3d785d35.png";
},{}],"setup/setup.ts":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.BRICK_ENERGY = exports.BRICK_IMAGES = exports.PADDLE_MAX_SPEED = exports.PADDLE_MIN_SPEED = exports.BALL_MAX_SPEED = exports.BALL_MIN_SPEED = exports.BRICKS_MAX_ARRAY = exports.BRICKS_MIN_ARRAY = exports.BRICKS_ARRAY_LENGTH = exports.BALL_STARTY = exports.BALL_STARTX = exports.BALL_SIZE = exports.PADDLE_Y_BOTTOM_LIMIT = exports.PADDLE_Y_TOP_LIMIT = exports.PADDLE_STARTX = exports.PADDLE_HEIGHT = exports.PADDLE_WIDTH = exports.PADDLE_PADDING = exports.BRICK_HEIGHT = exports.BRICK_WIDTH = exports.BRICK_PADDING = exports.STAGE_COLS = exports.STAGE_ROWS = exports.STAGE_PADDING = void 0;

var _brickRed = _interopRequireDefault(require("../assets/bricks/brick-red.png"));

var _brickBlue = _interopRequireDefault(require("../assets/bricks/brick-blue.png"));

var _brickGreen = _interopRequireDefault(require("../assets/bricks/brick-green.png"));

var _brickYellow = _interopRequireDefault(require("../assets/bricks/brick-yellow.png"));

var _brickPurple = _interopRequireDefault(require("../assets/bricks/brick-purple.png"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Grab the canvas element for calculating the brick width
// depending on canvas width
var canvas = document.querySelector('#playField'); // Constants

var STAGE_PADDING = 10;
exports.STAGE_PADDING = STAGE_PADDING;
var STAGE_ROWS = 20;
exports.STAGE_ROWS = STAGE_ROWS;
var STAGE_COLS = 10;
exports.STAGE_COLS = STAGE_COLS;
var BRICK_PADDING = 5;
exports.BRICK_PADDING = BRICK_PADDING;
var BRICK_WIDTH = Math.floor((canvas.width - STAGE_PADDING * 2) / STAGE_COLS) - BRICK_PADDING;
exports.BRICK_WIDTH = BRICK_WIDTH;
var BRICK_HEIGHT = Math.floor((canvas.height - STAGE_PADDING * 2) / STAGE_ROWS) - BRICK_PADDING;
exports.BRICK_HEIGHT = BRICK_HEIGHT;
var PADDLE_PADDING = 25;
exports.PADDLE_PADDING = PADDLE_PADDING;
var PADDLE_WIDTH = 70;
exports.PADDLE_WIDTH = PADDLE_WIDTH;
var PADDLE_HEIGHT = 10;
exports.PADDLE_HEIGHT = PADDLE_HEIGHT;
var PADDLE_STARTX = (canvas.width - PADDLE_WIDTH) / 2;
exports.PADDLE_STARTX = PADDLE_STARTX;
var PADDLE_Y_TOP_LIMIT = 450;
exports.PADDLE_Y_TOP_LIMIT = PADDLE_Y_TOP_LIMIT;
var PADDLE_Y_BOTTOM_LIMIT = canvas.height - PADDLE_HEIGHT - PADDLE_PADDING;
exports.PADDLE_Y_BOTTOM_LIMIT = PADDLE_Y_BOTTOM_LIMIT;
var BALL_SIZE = 15;
exports.BALL_SIZE = BALL_SIZE;
var BALL_STARTX = canvas.width - PADDLE_WIDTH;
exports.BALL_STARTX = BALL_STARTX;
var BALL_STARTY = canvas.height - PADDLE_HEIGHT - PADDLE_PADDING - BALL_SIZE;
exports.BALL_STARTY = BALL_STARTY;
var BRICKS_ARRAY_LENGTH = 9;
exports.BRICKS_ARRAY_LENGTH = BRICKS_ARRAY_LENGTH;
var BRICKS_MIN_ARRAY = 2;
exports.BRICKS_MIN_ARRAY = BRICKS_MIN_ARRAY;
var BRICKS_MAX_ARRAY = 9;
exports.BRICKS_MAX_ARRAY = BRICKS_MAX_ARRAY;
var BALL_MIN_SPEED = 5;
exports.BALL_MIN_SPEED = BALL_MIN_SPEED;
var BALL_MAX_SPEED = 10;
exports.BALL_MAX_SPEED = BALL_MAX_SPEED;
var PADDLE_MIN_SPEED = 6;
exports.PADDLE_MIN_SPEED = PADDLE_MIN_SPEED;
var PADDLE_MAX_SPEED = 12;
exports.PADDLE_MAX_SPEED = PADDLE_MAX_SPEED;
var BRICK_IMAGES = {
  1: _brickRed.default,
  2: _brickGreen.default,
  3: _brickYellow.default,
  4: _brickBlue.default,
  5: _brickPurple.default
};
exports.BRICK_IMAGES = BRICK_IMAGES;
var BRICK_ENERGY = {
  1: 1,
  2: 2,
  3: 4,
  4: 4,
  5: 8 // Purple brick

};
exports.BRICK_ENERGY = BRICK_ENERGY;
},{"../assets/bricks/brick-red.png":"assets/bricks/brick-red.png","../assets/bricks/brick-blue.png":"assets/bricks/brick-blue.png","../assets/bricks/brick-green.png":"assets/bricks/brick-green.png","../assets/bricks/brick-yellow.png":"assets/bricks/brick-yellow.png","../assets/bricks/brick-purple.png":"assets/bricks/brick-purple.png"}],"assets/ball.png":[function(require,module,exports) {
module.exports = "/ball.eb9fcda6.png";
},{}],"assets/paddle.png":[function(require,module,exports) {
module.exports = "/paddle.366ae955.png";
},{}],"game.ts":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Game = void 0;

var _Ball = require("./sprites/Ball");

var _Brick = require("./sprites/Brick");

var _Paddle = require("./sprites/Paddle");

var _Collison = require("./game/Collison");

var _helpers = require("./helpers");

var _setup = require("./setup/setup");

var _ball = _interopRequireDefault(require("./assets/ball.png"));

var _paddle = _interopRequireDefault(require("./assets/paddle.png"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var __spreadArrays = void 0 && (void 0).__spreadArrays || function () {
  for (var s = 0, i = 0, il = arguments.length; i < il; i++) {
    s += arguments[i].length;
  }

  for (var r = Array(s), k = 0, i = 0; i < il; i++) {
    for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++) {
      r[k] = a[j];
    }
  }

  return r;
};

var Game =
/** @class */
function () {
  function Game() {
    this.gameOver = false;
    this.score = 0;
    this.collision = new _Collison.Collision();
  }

  Game.prototype.start = function (playground, displayView) {
    this.gameOver = false;
    this.bricks = this.createBricks(new Array((_setup.BRICKS_MIN_ARRAY + (0, _helpers.randomInBoundaries)(0, _setup.BRICKS_MAX_ARRAY - _setup.BRICKS_MIN_ARRAY)) * _setup.BRICKS_ARRAY_LENGTH).fill(0).map(function () {
      return (0, _helpers.randomInBoundaries)(0, 5);
    }));
    var ballSpeed = (0, _helpers.randomInBoundaries)(_setup.BALL_MIN_SPEED, _setup.BALL_MAX_SPEED);
    var paddleSpeed = (0, _helpers.randomInBoundaries)(_setup.PADDLE_MIN_SPEED, _setup.PADDLE_MAX_SPEED);
    this.ball = new _Ball.Ball(_setup.BALL_SIZE, ballSpeed, {
      x: _setup.BALL_STARTX,
      y: _setup.BALL_STARTY
    }, _ball.default);
    this.paddle = new _Paddle.Paddle(paddleSpeed, _setup.PADDLE_WIDTH, _setup.PADDLE_HEIGHT, {
      x: _setup.PADDLE_STARTX,
      y: playground.canvas.height - _setup.PADDLE_HEIGHT - _setup.PADDLE_PADDING
    }, _paddle.default);
    displayView.score = this.score;
    displayView.setGameMode();
    displayView.ballSpeed = ballSpeed;
    displayView.paddleSpeed = paddleSpeed;
    this.loop(playground, displayView);
  };

  Game.prototype.loop = function (playground, displayView) {
    var _this = this;

    playground.clear();
    playground.drawBricks(this.bricks);
    playground.drawSprite(this.paddle);
    playground.drawSprite(this.ball); // Move Ball

    this.ball.moveBall(); // Move paddle and check so it won't exit the playfield

    if (this.canMovePaddle(playground)) {
      this.paddle.movePaddle();
    }

    this.collision.checkBallCollision(this.ball, this.paddle, playground);
    var collidingBrick = this.collision.isCollidingBricks(this.ball, this.bricks);

    if (collidingBrick) {
      this.score += 1;
      displayView.score = this.score;
    } // Game Over when ball leaves playField


    if (this.ball.pos.y > playground.canvas.height) {
      this.gameOver = true;
      displayView.score = this.score;
      displayView.highScore = displayView.highScore < this.score ? this.score : displayView.highScore;
      this.score = 0;
    } // If game won, set gameOver and display win


    if (this.bricks.length === 0) {
      displayView.info = 'Game Won!';
      this.gameOver = false;
      displayView.setDefaultMode();
      return;
    } // Return if gameover and don't run the requestAnimationFrame


    if (this.gameOver) {
      displayView.info = 'Game Over!';
      this.gameOver = true;
      displayView.setDefaultMode();
      return;
    }

    requestAnimationFrame(function () {
      return _this.loop(playground, displayView);
    });
  };

  Game.prototype.createBricks = function (array) {
    return array.reduce(function (ack, element, i) {
      var row = Math.floor((i + 1) / _setup.STAGE_COLS);
      var col = i % _setup.STAGE_COLS;
      var x = _setup.STAGE_PADDING + col * (_setup.BRICK_WIDTH + _setup.BRICK_PADDING);
      var y = _setup.STAGE_PADDING + row * (_setup.BRICK_HEIGHT + _setup.BRICK_PADDING);
      if (element === 0) return ack;
      return __spreadArrays(ack, [new _Brick.Brick(_setup.BRICK_ENERGY[element], _setup.BRICK_WIDTH, _setup.BRICK_HEIGHT, {
        x: x,
        y: y
      }, _setup.BRICK_IMAGES[element])]);
    }, []);
  };

  ;

  Game.prototype.canMovePaddle = function (playground) {
    return (!this.paddle.isMovingLeft || this.paddle.isMovingLeft && this.paddle.pos.x > 0) && (!this.paddle.isMovingRight || this.paddle.isMovingRight && this.paddle.pos.x < playground.canvas.width - this.paddle.width) && (!this.paddle.isMovingTop || this.paddle.isMovingTop && this.paddle.pos.y > _setup.PADDLE_Y_TOP_LIMIT) && (!this.paddle.isMovingDown || this.paddle.isMovingDown && this.paddle.pos.y < _setup.PADDLE_Y_BOTTOM_LIMIT);
  };

  return Game;
}();

exports.Game = Game;
},{"./sprites/Ball":"sprites/Ball.ts","./sprites/Brick":"sprites/Brick.ts","./sprites/Paddle":"sprites/Paddle.ts","./game/Collison":"game/Collison.ts","./helpers":"helpers.ts","./setup/setup":"setup/setup.ts","./assets/ball.png":"assets/ball.png","./assets/paddle.png":"assets/paddle.png"}],"view/CanvasView.ts":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CanvasView = void 0;

var CanvasView =
/** @class */
function () {
  function CanvasView(canvasName) {
    this.canvas = document.querySelector(canvasName);
    this.context = this.canvas.getContext('2d');
  }

  CanvasView.prototype.clear = function () {
    var _a;

    (_a = this.context) === null || _a === void 0 ? void 0 : _a.clearRect(0, 0, this.canvas.width, this.canvas.height);
  };

  CanvasView.prototype.drawSprite = function (sprite) {
    var _a;

    if (!sprite) return;
    (_a = this.context) === null || _a === void 0 ? void 0 : _a.drawImage(sprite.image, sprite.pos.x, sprite.pos.y, sprite.width, sprite.height);
  };

  CanvasView.prototype.drawBricks = function (bricks) {
    var _this = this;

    bricks.forEach(function (brick) {
      return _this.drawSprite(brick);
    });
  };

  return CanvasView;
}();

exports.CanvasView = CanvasView;
},{}],"view/DisplayView.ts":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DisplayView = void 0;

var _helpers = require("../helpers");

var _setup = require("../setup/setup");

var DisplayView =
/** @class */
function () {
  function DisplayView(containerName) {
    this.scorePrefix = '0000000';
    this.container = document.querySelector(containerName);
    this.clear();
    this.renderScore(this.highScore || 0, 'High Score');
    this.renderScore(0, 'Score');
    this.renderBallSpeed(0);
    this.renderPaddleSpeed(0);
    this.renderInfo('Press Start to play!');
    this.renderStartButton();
  }

  DisplayView.prototype.renderScore = function (data, label) {
    var info = (0, _helpers.createElement)('div', this.container, {
      className: 'main__info'
    });
    (0, _helpers.createElement)('div', info, {
      className: 'main__heading',
      textContent: label
    });

    if (label === 'Score') {
      this._score = (0, _helpers.createElement)('div', info, {
        className: 'main__text',
        textContent: this.parseScore(data)
      });
    } else {
      this._highScore = (0, _helpers.createElement)('div', info, {
        className: 'main__text',
        textContent: this.parseScore(data)
      });
    }
  };

  DisplayView.prototype.renderBallSpeed = function (data) {
    var speed = (0, _helpers.createElement)('div', this.container, {
      className: 'main__info'
    });
    (0, _helpers.createElement)('div', speed, {
      className: 'main__heading',
      textContent: 'Ball Speed'
    });
    this._ballSpeed = (0, _helpers.createElement)('div', speed, {
      className: 'main__text',
      textContent: this.parseSpeed(data, _setup.BALL_MIN_SPEED)
    });
  };

  DisplayView.prototype.renderPaddleSpeed = function (data) {
    var speed = (0, _helpers.createElement)('div', this.container, {
      className: 'main__info'
    });
    (0, _helpers.createElement)('div', speed, {
      className: 'main__heading',
      textContent: 'Paddle Speed'
    });
    this._paddleSpeed = (0, _helpers.createElement)('div', speed, {
      className: 'main__text',
      textContent: this.parseSpeed(data, _setup.PADDLE_MIN_SPEED)
    });
  };

  DisplayView.prototype.renderInfo = function (text) {
    var info = (0, _helpers.createElement)('div', this.container, {
      className: 'main__info'
    });
    this._info = (0, _helpers.createElement)('div', info, {
      className: 'main__heading',
      textContent: text
    });
  };

  DisplayView.prototype.renderStartButton = function () {
    this._start = (0, _helpers.createElement)('button', this.container, {
      className: 'main__start-button',
      textContent: 'Start'
    });
  };

  Object.defineProperty(DisplayView.prototype, "ballSpeed", {
    set: function set(data) {
      this._ballSpeed.innerText = this.parseSpeed(data, _setup.BALL_MIN_SPEED);
    },
    enumerable: false,
    configurable: true
  });
  Object.defineProperty(DisplayView.prototype, "paddleSpeed", {
    set: function set(data) {
      this._paddleSpeed.innerText = this.parseSpeed(data, _setup.PADDLE_MIN_SPEED);
    },
    enumerable: false,
    configurable: true
  });
  Object.defineProperty(DisplayView.prototype, "startButton", {
    get: function get() {
      return this._start;
    },
    enumerable: false,
    configurable: true
  });
  Object.defineProperty(DisplayView.prototype, "highScore", {
    get: function get() {
      return Number(localStorage.getItem('high')) || 0;
    },
    set: function set(data) {
      localStorage.setItem('high', data.toString());
      this._highScore.innerText = this.parseScore(data).toString();
    },
    enumerable: false,
    configurable: true
  });
  Object.defineProperty(DisplayView.prototype, "score", {
    set: function set(data) {
      this._score.innerText = this.parseScore(data).toString();
    },
    enumerable: false,
    configurable: true
  });
  Object.defineProperty(DisplayView.prototype, "info", {
    set: function set(text) {
      this._info.innerText = text;
    },
    enumerable: false,
    configurable: true
  });

  DisplayView.prototype.setGameMode = function () {
    this._start.classList.add('hide');

    this._info.classList.add('hide');
  };

  DisplayView.prototype.setDefaultMode = function () {
    this._start.classList.remove('hide');

    this._info.classList.remove('hide');
  };

  DisplayView.prototype.parseSpeed = function (data, min) {
    return (data / min).toFixed(2) + "x";
  };

  DisplayView.prototype.parseScore = function (data) {
    return "" + (this.scorePrefix + data).slice(-this.scorePrefix.length);
  };

  DisplayView.prototype.clear = function () {
    while (this.container.firstChild) {
      this.container.firstChild.remove();
    }
  };

  return DisplayView;
}();

exports.DisplayView = DisplayView;
},{"../helpers":"helpers.ts","../setup/setup":"setup/setup.ts"}],"index.ts":[function(require,module,exports) {
"use strict";

var _game = require("./game");

var _CanvasView = require("./view/CanvasView");

var _DisplayView = require("./view/DisplayView");

var canvas = new _CanvasView.CanvasView('#playField');
var displayView = new _DisplayView.DisplayView('#display');
var game = new _game.Game();
displayView.startButton.addEventListener('click', function () {
  game.start(canvas, displayView);
});
},{"./game":"game.ts","./view/CanvasView":"view/CanvasView.ts","./view/DisplayView":"view/DisplayView.ts"}],"../node_modules/parcel-bundler/src/builtins/hmr-runtime.js":[function(require,module,exports) {
var global = arguments[3];
var OVERLAY_ID = '__parcel__error__overlay__';
var OldModule = module.bundle.Module;

function Module(moduleName) {
  OldModule.call(this, moduleName);
  this.hot = {
    data: module.bundle.hotData,
    _acceptCallbacks: [],
    _disposeCallbacks: [],
    accept: function (fn) {
      this._acceptCallbacks.push(fn || function () {});
    },
    dispose: function (fn) {
      this._disposeCallbacks.push(fn);
    }
  };
  module.bundle.hotData = null;
}

module.bundle.Module = Module;
var checkedAssets, assetsToAccept;
var parent = module.bundle.parent;

if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== 'undefined') {
  var hostname = "" || location.hostname;
  var protocol = location.protocol === 'https:' ? 'wss' : 'ws';
  var ws = new WebSocket(protocol + '://' + hostname + ':' + "63427" + '/');

  ws.onmessage = function (event) {
    checkedAssets = {};
    assetsToAccept = [];
    var data = JSON.parse(event.data);

    if (data.type === 'update') {
      var handled = false;
      data.assets.forEach(function (asset) {
        if (!asset.isNew) {
          var didAccept = hmrAcceptCheck(global.parcelRequire, asset.id);

          if (didAccept) {
            handled = true;
          }
        }
      }); // Enable HMR for CSS by default.

      handled = handled || data.assets.every(function (asset) {
        return asset.type === 'css' && asset.generated.js;
      });

      if (handled) {
        console.clear();
        data.assets.forEach(function (asset) {
          hmrApply(global.parcelRequire, asset);
        });
        assetsToAccept.forEach(function (v) {
          hmrAcceptRun(v[0], v[1]);
        });
      } else if (location.reload) {
        // `location` global exists in a web worker context but lacks `.reload()` function.
        location.reload();
      }
    }

    if (data.type === 'reload') {
      ws.close();

      ws.onclose = function () {
        location.reload();
      };
    }

    if (data.type === 'error-resolved') {
      console.log('[parcel] ✨ Error resolved');
      removeErrorOverlay();
    }

    if (data.type === 'error') {
      console.error('[parcel] 🚨  ' + data.error.message + '\n' + data.error.stack);
      removeErrorOverlay();
      var overlay = createErrorOverlay(data);
      document.body.appendChild(overlay);
    }
  };
}

function removeErrorOverlay() {
  var overlay = document.getElementById(OVERLAY_ID);

  if (overlay) {
    overlay.remove();
  }
}

function createErrorOverlay(data) {
  var overlay = document.createElement('div');
  overlay.id = OVERLAY_ID; // html encode message and stack trace

  var message = document.createElement('div');
  var stackTrace = document.createElement('pre');
  message.innerText = data.error.message;
  stackTrace.innerText = data.error.stack;
  overlay.innerHTML = '<div style="background: black; font-size: 16px; color: white; position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; padding: 30px; opacity: 0.85; font-family: Menlo, Consolas, monospace; z-index: 9999;">' + '<span style="background: red; padding: 2px 4px; border-radius: 2px;">ERROR</span>' + '<span style="top: 2px; margin-left: 5px; position: relative;">🚨</span>' + '<div style="font-size: 18px; font-weight: bold; margin-top: 20px;">' + message.innerHTML + '</div>' + '<pre>' + stackTrace.innerHTML + '</pre>' + '</div>';
  return overlay;
}

function getParents(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return [];
  }

  var parents = [];
  var k, d, dep;

  for (k in modules) {
    for (d in modules[k][1]) {
      dep = modules[k][1][d];

      if (dep === id || Array.isArray(dep) && dep[dep.length - 1] === id) {
        parents.push(k);
      }
    }
  }

  if (bundle.parent) {
    parents = parents.concat(getParents(bundle.parent, id));
  }

  return parents;
}

function hmrApply(bundle, asset) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (modules[asset.id] || !bundle.parent) {
    var fn = new Function('require', 'module', 'exports', asset.generated.js);
    asset.isNew = !modules[asset.id];
    modules[asset.id] = [fn, asset.deps];
  } else if (bundle.parent) {
    hmrApply(bundle.parent, asset);
  }
}

function hmrAcceptCheck(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (!modules[id] && bundle.parent) {
    return hmrAcceptCheck(bundle.parent, id);
  }

  if (checkedAssets[id]) {
    return;
  }

  checkedAssets[id] = true;
  var cached = bundle.cache[id];
  assetsToAccept.push([bundle, id]);

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    return true;
  }

  return getParents(global.parcelRequire, id).some(function (id) {
    return hmrAcceptCheck(global.parcelRequire, id);
  });
}

function hmrAcceptRun(bundle, id) {
  var cached = bundle.cache[id];
  bundle.hotData = {};

  if (cached) {
    cached.hot.data = bundle.hotData;
  }

  if (cached && cached.hot && cached.hot._disposeCallbacks.length) {
    cached.hot._disposeCallbacks.forEach(function (cb) {
      cb(bundle.hotData);
    });
  }

  delete bundle.cache[id];
  bundle(id);
  cached = bundle.cache[id];

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    cached.hot._acceptCallbacks.forEach(function (cb) {
      cb();
    });

    return true;
  }
}
},{}]},{},["../node_modules/parcel-bundler/src/builtins/hmr-runtime.js","index.ts"], null)
//# sourceMappingURL=/src.77de5100.js.map